(function(NS) {
  var image = new Image();
  image.src = "images/robot.png";



  var Shell = function() {
    Particle.apply(this, arguments);
  };

  Shell.prototype = _.defaults({
    dX: 0,
    dY: 0,
    name: 'shell',

    tick: function(world) {
      var i;
      this.x = this.x + this.dX;
      this.y = this.y + this.dY;
      this.realX = this.x;
      this.realY = this.y;

      var objects = world.objects.intersects(this, false);
      if (objects.length > 0) {
        for (i = 0; i < objects.length; i++) {
          var parent = objects[i];
          while (parent !== undefined) {
            if (parent.hit) {
              parent.hit(this);
              break;
            }
            parent = parent.parent;
          }
        }
        world.removeParticle(this);
      }
    },

    render: function(context) {
      context.beginPath();
      context.strokeStyle='#800000';
      context.arc(this.x, this.y, 5, 0, Math.PI*2);
      context.stroke();
    }
  }, NS.Particle.prototype);
  
  var Cannon = function(robot) {
    this.name = 'cannon';
    this.robot = robot;
    this.angle = 0;
    this.cooldown = 0;
  };

  Cannon.prototype = {
    fire: function() {
      if (this.cooldown === 0) {
        var a = this.angle / 180 * Math.PI;
        var dX = Math.cos(a);
        var dY = Math.sin(a);
        var shell = new Shell({ x: this.robot.realX + 21 * dX, y: this.robot.realY + 21 * dY, dX: dX * 5, dY: dY * 5 });
        this.robot.world.addParticle(shell);
        this.cooldown = 10;
      }
    },
    tick: function() {
      if (this.cooldown > 0) {
        this.cooldown --;
      }
    },
    draw: function() {},
    api: function() {
      var that = this;
      return {
        fire: function() {
          that.fire();
        },
        ready: function() {
          return that.cooldown === 0;
        },
        aim: function(angle) {
          if (angle === undefined) {
            return that.angle;
          } else {
            that.angle = angle;
          }
        }
      };
    }
  };

  var Thrusters = function(robot) {
    this.name = 'thrusters';
    this.robot = robot;
    this.left = false;
    this.right = false;
    this.top = false;
    this.bottom = false;
  };

  Thrusters.prototype = {
    frame: 0,

    tick: function(world) {
      var velocityX = this.left - this.right;
      var velocityY = this.top - this.bottom;
      this.robot.move(this.robot.xPos + velocityX * 2, this.robot.yPos + velocityY * 2);
      this.frame++;
    },

    _drawThruster: function(context) {
      context.save();
      context.globalCompositeOperation = 'lighter';
      for (var i = 0; i < 20; i++) {
        context.fillStyle = "rgba(" + "180, 160, 20,"+ i/20+")";
        context.strokeStyle = "rgba(" + "180, 80, 20," +  i/20 + ")";
        for (var j = 0; j < i; j++) {
          var x = -40 + i;
          var w = i < 15 ? i * 1.1 : (20 - i) * 3 + 3;
          var y = j / i * w - w / 2;
          var x1 = (x - Math.random() * 6)|0;
          var y1 = (y + Math.random() * 4 -2)|0;
          context.beginPath();
          context.moveTo(x1, y1);
          context.lineTo((x - Math.random() * 6)|0, (y + Math.random() * 4 - 2)|0);
          context.lineTo((x - Math.random() * 6)|0, (y + Math.random() * 4 - 2)|0);
          context.lineTo(x1, y1);
          context.fill();
          context.stroke();
        }
      }
      context.restore();
    },
    illuminate: function(world) {
      if (this.left) {
        world.drawLighting(this.robot.realX -30, this.robot.realY, '#a07030');
      }
      if (this.right) {
        world.drawLighting(this.robot.realX + 30, this.robot.realY, '#a07030');
      }
      if (this.top) {
        world.drawLighting(this.robot.realX, this.robot.realY - 30, '#a07030');
      }
      if (this.bottom) {
        world.drawLighting(this.robot.realX, this.robot.realY + 30, '#a07030');
      }
    },
    draw: function(context) {
      var world = this.robot.world;
      var overlay = world.contexts.overlay;
      var which = (this.frame % 10);

      if (this.left) {
        overlay.drawImage(Thrusters.imageData.left[which], this.robot.realX - 51.5, this.robot.realY - 20.5);
      }
      if (this.right) {
        overlay.drawImage(Thrusters.imageData.right[which], this.robot.realX + 12.5, this.robot.realY - 20.5);
      }
      if (this.top) {
        overlay.drawImage(Thrusters.imageData.top[which], this.robot.realX - 20.5, this.robot.realY - 51.5);
      }
      if (this.bottom) {
        overlay.drawImage(Thrusters.imageData.bottom[which], this.robot.realX - 20.5, this.robot.realY + 12.5);
      }
    },

    api: function() {
      var that = this;
      return {
        left: function(status) {
          if (status === undefined) {
            return that.left;
          } else {
            that.left = (status === true);
          }
        },
        right: function(status) {
          if (status === undefined) {
            return that.right;
          } else {
            that.right = (status === true);
          }
        },
        top: function(status) {
          if (status === undefined) {
            return that.top;
          } else {
            that.top = (status === true);
          }
        },
        bottom: function(status) {
          if (status === undefined) {
            return that.bottom;
          } else {
            that.bottom = (status === true);
          }
        }
      }
    }
  }


  var Radar = function(robot) {
    this.name = 'radar';
    this.robot = robot;
    this.active = false;
    this.hit = false;
    this.angle = 0;
    this.stack = [];
    this.drawStack = [];
  }

  Radar.prototype = {
    draw: function(context) {
      var ds = this.drawStack;
      if (ds.length > 0) {
        var overlay = this.robot.world.contexts.overlay;
        overlay.beginPath();
        overlay.strokeStyle = 'rgba(255,80,80,0.5)';
        overlay.lineWidth = 3;
        while (ds.length > 0) {
          var active = ds.shift();
          var intercept = active.intercept;
          var x = intercept.x - this.robot.realX;
          var y = intercept.y - this.robot.realY;
          overlay.moveTo(this.robot.realX, this.robot.realY);
          overlay.lineTo(intercept.x, intercept.y);
        }
        overlay.stroke();
      }
    },
    fire: function(angle) {
      var a = angle / 180 * Math.PI;
      var dX = Math.cos(a);
      var dY = Math.sin(a);
    
      var ray = { p1: { x: this.robot.realX + dX * 20, y: this.robot.realY + dY * 20 }, p2: {x: this.robot.realX + dX * 21, y: this.robot.realY + dY * 21 }};

      return this.robot.world.nearest(ray);
    },
    tick: function() {
      if (this.stack.length > 0) {
        var active = this.stack[0];
        if (active.response === undefined) {
          var nearest = this.fire(active.angle);
          if (nearest) {
            active.distance = Math.sqrt(nearest.d),
            active.intercept = nearest.i;
            active.response = Math.min(active.distance >> 7, 5);
            this.drawStack.push(active);
          } else {
            active.response = 5;
          }
        } else {
          if (active.response <= 0) {
            if (active.distance == undefined) {
              this.robot.trigger('radar:miss', active.angle);
            } else {
              this.robot.trigger('radar:hit', active.angle, active.distance);
            }
            this.stack.shift();
          } else {
            active.response--;
          }
        }
      }
    },

    api: function() {
      var that = this;
      return {
        ping: function() {
          that.stack.push({
            angle: that.angle
          });
        },
        angle: function(angle) {
          if (angle === undefined) {
            return that.angle;
          } else {
            that.angle = angle |0;
          }
        }
      }
    }
  }

  var Sensor = function(robot, transform, name) {
    this.touching = false;
    this.touched = false;

    this.name = name;
    this.transform = transform;
    this.robot = robot;
    this.parent = robot;
    this.width = 25;
    this.height = 5;
    this.visible = true;
    this.passable = false;

    NS.Obj.call(this);

    var unit = this._applyTransform(this.transform, 0, 1);
    var center = this._applyTransform(this.transform, 0,0 );
    unit[0] -= center[0];
    unit[1] -= center[1];

    this.surfaceVector = unit;

    robot.addChild(this);
    this._update();
  };

  Sensor.prototype = _.defaults({
    onContact: function(object, robot) {
      if (! object.passable) {
        var xDir = this.surfaceVector[0];
        var yDir = this.surfaceVector[1];

        var diffX = (object.realX - (object.boundWidth / 2 * xDir)) - (this.realX + (this.boundWidth / 2 * xDir));
        var diffY = (object.realY - (object.boundHeight / 2 * yDir)) - (this.realY + (this.boundHeight / 2 * yDir));

        diffX = diffX * xDir;
        diffY = diffY * yDir;
        // Good enough contact management.  Anything more and I may as well find a
        // library to do this for me.
        if ((diffX <= 0 && xDir !== 0) || (diffY <= 0 && yDir !== 0)) {
          var parent = object;
          while (parent != undefined && !parent.moveable) {
            parent = parent.parent;
          }
          if (parent && parent.moveable) {
            diffX = diffX * xDir / 4;
            diffY = diffY * yDir / 4;
            this.robot.move(this.robot.xPos + diffX, this.robot.yPos + diffY);
            parent.move(parent.xPos - diffX, parent.yPos - diffY);
          } else {
            this.robot.move(this.robot.xPos + diffX * xDir, this.robot.yPos + diffY * yDir);
          }

          if (! this.touching) {
            this.robot.trigger('sensor:' + this.name, true);
            this.touching = true;
          }
          this.touched = true;
        } 
      }
    },
    draw: function() {},
    tick: function() {
      if (this.touched != this.touching) {
        this.touching = this.touched;
        this.robot.trigger('sensor:' + this.name, false);
      }
      if (this.touched) {
        this.touched = false;
      }
    }
  }, NS.Obj.prototype);

  var Robot = function() {
    NS.Obj.apply(this, arguments);

    this.fuel = 0;
    this.name = this.name === undefined ? 'robot' : this.name;
    this.width = 0;
    this.height = 0;
    this.invisible = false;
    this.passable = true;
    this.moveable = true;

    this.addChild(new Box({name: 'body', width: 20, height: 20, invisible: false, passable: false }));
    this.components = [new Thrusters(this), new Radar(this), new Cannon(this)]; //, 
    // TODO: Fix component tick so objects can be components too
    [
      new Sensor(this, [[1,0,0], [0,1,17.5], [0,0,1]], 'bottom'),
      new Sensor(this, [[-1,0,0], [0,-1,-17.5], [0,0,1]], 'top'),
      new Sensor(this, [[0,1,17.5], [-1,0,0], [0,0,1]], 'right'),
      new Sensor(this, [[0,-1,-17.5], [1,0,0], [0,0,1]], 'left')];

      _.extend(this, Backbone.Events);
      this.evalCode();
  };

  Robot.prototype = _.defaults({
    xPos: 100,
    yPos: 100,
    started: false,
    color: '#f09090',

    illuminate: function(world) {
      // Loop over components? 
    },
    setCode: function(code) {
      this.code = code;
      this.evalCode();
    },

    hit: function(object) {
      console.log(this.name + ' hit by ' + object.name);
    },

    evalCode: function() {
      // turn off all event listeners
      this.off();

      if (this.code) {
        var workspace = { 
          once: function(name,fn,ctx) {
            if (ctx === undefined) {
              ctx = workspace;
            }
            this.once(name, fn, ctx);
          }.bind(this),
          on: function(name,fn,ctx) {
            if (ctx === undefined) {
              ctx = workspace;
            }
            this.on(name, fn, ctx);
          }.bind(this),
          off: function(name, fn, ctx) {
            this.off(name, fn, ctx);
          }.bind(this)
        };

        for (var i = 0; i < this.components.length; i++) {
          var component = this.components[i];
          if (component.api) {
            workspace[component.name] = component.api();
          }
        }

        try {
          var fn = eval('[function() { var setTimeout = "", window = "", document = "", setInterval = "";' + this.code + '}]')[0].call(workspace);
          this.logic = fn;
        } catch (e) {
          console.log(e);
          return;
        }
      }
    },

    tick: function() {
      if (!this.started) {
        this.trigger('start');
        this.started = true;
        return;
      }
      for (var i = 0; i < this.components.length; i++) {
        var component = this.components[i];
        component.tick();
      }
    },
    move: function(x, y) {
      this.xPos = x;
      this.yPos = y;
      Obj.prototype.move.call(this, x, y);
    },
    draw: function(context) {
      context.drawImage(image, -image.width / 2, -image.height / 2); 
      for (var i = 0; i < this.components.length; i++) {
        var component = this.components[i];
        component.draw(context);
      }

      var overlay = this.world.contexts.overlay;
      overlay.beginPath();
      overlay.strokeStyle = '#d0d0d0';
      overlay.fillStyle = '#d0d0d0';
      overlay.lineWidth = 1;
      overlay.arc(this.realX, this.realY - 2, 6, 0, Math.PI * 2);
      overlay.fill();
      overlay.stroke();

      overlay.beginPath();
      overlay.strokeStyle = this.color;
      overlay.lineWidth = 3;
      overlay.fillStyle = '#e0e0e0';
      overlay.arc(this.realX, this.realY - 2, 4, 0, Math.PI * 2);
      overlay.fill();
      overlay.stroke();
    }
  }, NS.Obj.prototype);


  NS.Robot = Robot;

  $(document).ready( function() {
    var thrusters = {
      left: [],
      right: [],
      top: [],
      bottom: []
    };

    var variations;
    for (var l = 0; l < 4; l ++) {
      switch(l) {
        case 0:
          variations = thrusters['left'];
        break;
        case 1:
          variations = thrusters['bottom'];
        break;
        case 2:
          variations = thrusters['top'];
        break;
        case 3:
          variations = thrusters['right'];
        break;
      }

      for (var k = 0; k < 10; k++) {
        var canvas = document.createElement('canvas');
        canvas.width = 40;
        canvas.height = 40;

        variations.push(canvas);

        var context = canvas.getContext('2d');
        context.clearRect(0,0,39,39);
        context.lineWidth = 2.0;
        context.globalCompositeOperation = 'lighter';

        switch (l) {
          case 0:
            context.setTransform(1,0,0,1,20.5,20.5);
          break;
          case 1: 
            context.setTransform(0,-1,1,0,20.5,20.5);
          break;
          case 2:
            context.setTransform(0,1,-1,0,20.5,20.5);
          break;
          case 3:
            context.setTransform(-1,0,0,-1,20.5,20.5);
        }
        for (var i = 0; i < 20; i++) {
          context.fillStyle = "rgba(" + "180, 160, 20,"+ i/20+")";
          context.strokeStyle = "rgba(" + "180, 80, 20," +  i/20 + ")";
          for (var j = 0; j < i; j++) {
            var x = i - 7;
            var w = i < 15 ? i * 1.1 : (20 - i) * 3 + 3;
            var y = j / i * w - w / 2;
            var x1 = (x - Math.random() * 6)|0;
            var y1 = (y + Math.random() * 4 -2)|0;
            context.beginPath();
            context.moveTo(x1, y1);
            context.lineTo((x - Math.random() * 6)|0, (y + Math.random() * 4 - 2)|0);
            context.lineTo((x - Math.random() * 6)|0, (y + Math.random() * 4 - 2)|0);
            context.lineTo(x1, y1);
            context.fill();
            context.stroke();
          }
        }
      }
    }
    Thrusters.imageData = thrusters;
  });
}(window));

