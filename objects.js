(function(NS) {
  var Obj = NS.Obj;

  // Surface object stolen from Robot (Sensor). It's now officially probably a
  // good idea to use a physics library.
  var Surface = function(parent, transform, name) {
    this.name = name;
    this.transform = transform;
    this.parent = parent;
    this.width = 25;
    this.height = 5;
    this.visible = false;
    this.passable = false;

    NS.Obj.call(this);

    var unit = this._applyTransform(this.transform, 0, 1);
    var center = this._applyTransform(this.transform, 0,0 );
    unit[0] -= center[0];
    unit[1] -= center[1];

    this.surfaceVector = unit;

    parent.addChild(this);
    this._update();
  };

  Surface.prototype = _.defaults({
    onContact: function(object) {
      if (! object.passable) {
        var xDir = this.surfaceVector[0];
        var yDir = this.surfaceVector[1];

        var diffX = (object.realX - (object.boundWidth / 2 * xDir)) - (this.realX + (this.boundWidth / 2 * xDir));
        var diffY = (object.realY - (object.boundHeight / 2 * yDir)) - (this.realY + (this.boundHeight / 2 * yDir));

        diffX = diffX * xDir;
        diffY = diffY * yDir;
        // Good enough contact management.  Anything more and I may as well find a
        // library to do this for me.
        if ((diffX <= 0 && xDir !== 0) || (diffY <= 0 && yDir !== 0)) {
          var parent = object;
          while (parent != undefined && !parent.moveable) {
            parent = parent.parent;
          }
          if (parent && parent.moveable) {
            diffX = diffX * xDir / 2;
            diffY = diffY * yDir / 2;
            this.parent.move(this.parent.xPos + diffX, this.parent.yPos + diffY);
            parent.move(parent.xPos - diffX, parent.yPos - diffY);
          } else {
            this.parent.move(this.parent.xPos + diffX * xDir, this.parent.yPos + diffY * yDir);
          }
        }
      }
    }
  }, Obj.prototype);


  var Crate = function() {
    Obj.apply(this, arguments);
    this.invisible = false;
    this.moveable = true;
    this.passable = false;

    var surfaces = [new Surface(this, [[1,0,0], [0,1,17.5], [0,0,1]], 'bottom'),
      new Surface(this, [[-1,0,0], [0,-1,-17.5], [0,0,1]], 'top'),
      new Surface(this, [[0,1,17.5], [-1,0,0], [0,0,1]], 'right'),
      new Surface(this, [[0,-1,-17.5], [1,0,0], [0,0,1]], 'left')];


  };

  Crate.prototype = _.defaults({
    move: function(x, y) {
      this.xPos = x;
      this.yPos = y;
      Obj.prototype.move.call(this, x, y);
    },
    draw: function(context) {
      context.beginPath();
      context.rect(-this.hWidth, -this.hHeight, this.width, this.height);
      context.strokeStyle = '#e0e0e0';
      context.fillStyle = '#d0d0d0';
      context.fill();
      context.stroke();

      context.beginPath();
      context.rect(-this.hWidth + 5, -this.hHeight + 5, this.width - 10, this.height - 10);
      context.strokeStyle = '#b0b0b0';
      context.fillStyle = '#c0c0c0';
      context.fill();
      context.stroke();


    }
  }, Obj.prototype);


  var Switch = function() {
    Obj.apply(this, arguments);
    this.invisible = false;
    this.isOn = false;
    this.activated = false;
    this.contact = false;
    this.passable = true;
  };

  Switch.prototype = _.defaults({
    toggle: function(world) {
      this.isOn = !this.isOn;
      if (this.isOn && this.onOn) {
        this.onOn();
      } else if (!this.isOn && this.onOff) {
        this.onOff();
      }
    },
    onCovered: function(what, world) {
      this.activated = true;
      if (this.contact) {
        return;
      }
      this.toggle(world);
      this.contact = true;
    },
    tick: function() {
      if (!this.activated && this.contact) {
        this.contact = false;
      }
      this.activated = false;
    },
    illuminate: function(world) {
      if (this.isOn) {
        world.drawLighting(this.realX, this.realY, '#00e000');
      } else {
        world.drawLighting(this.realX, this.realY, '#e00000');
      }
    },
    draw: function(context) {
      context.beginPath();
      if (this.isOn) {
        context.fillStyle = '#a0c0a0';
        context.strokeStyle = '#809080';
      } else {
        context.fillStyle = '#c0a0a0';
        context.strokeStyle = '#908080';
      }

      context.rect(-this.hWidth, -this.hHeight, this.width, this.height);
      context.fill();
      context.stroke();

      context.beginPath();
      if (this.isOn) {
        context.strokeStyle = '#004000';
        context.fillStyle = '#40ff40';
      } else {
        context.strokeStyle = '#400000';
        context.fillStyle = '#ff4040';
      }
      context.rect(-this.hWidth + 5, -this.hHeight + 5, this.width - 10, this.height - 10);
      context.fill();
      context.stroke();
    }
  }, Obj.prototype);

  var Button = function() {
    Obj.apply(this, arguments);
    this.invisible = false;
    this.isOn = false;
    this.activated = false;
    this.covered = false;
    this.passable = true;
  };

  Button.prototype = _.defaults({
    onCovered: function(what, world) {
      this.covered = true;
      if (!this.activated) {
        this.activated = true;
        this.isOn = true;
        this.onOn();
      }
    },
    tick: function() {
      if (!this.covered && this.activated) {
        this.isOn = false;
        this.activated = false;
        this.onOff();
      }
      this.covered = false;
    },
    illuminate: function(world) {
      if (this.isOn) {
        world.drawLighting(this.realX, this.realY, '#00e000');
      } else {
        world.drawLighting(this.realX, this.realY, '#e00000');
      }
    },
    draw: function(context) {
      context.beginPath();
      if (this.isOn) {
        context.fillStyle = '#a0c0a0';
        context.strokeStyle = '#809080';
      } else {
        context.fillStyle = '#c0a0a0';
        context.strokeStyle = '#908080';
      }

      context.arc(0, 0, this.hWidth, 0, Math.PI * 2);
      context.fill();
      context.stroke();

      context.beginPath();
      if (this.isOn) {
        context.strokeStyle = '#004000';
        context.fillStyle = '#40ff40';
      } else {
        context.strokeStyle = '#400000';
        context.fillStyle = '#ff4040';
      }
      context.arc(0, 0, this.hWidth - 5, 0, Math.PI * 2);
      context.fill();
      context.stroke();
    }
  }, Obj.prototype);

  var Light = function() {
    Box.apply(this, arguments);
    this.invisible = false;
    this.passable = true;
    this.timer = 0;
    this.isOn = this.isOn === undefined ? true : this.isOn;
  };

  Light.prototype = _.defaults({
    illuminate: function(world) {
      if (this.isOn) {
        world.drawLighting(this.realX, this.realY, '#405050');
      }
    },
    setOn: function(on) {
      if (on !== this.isOn) {
        this.isOn = on;
      }
    },
    draw: function(context) {
    },
    tick: function() {
      if (this.strobe) {
        if (this.strobePos !== undefined) {
          this.setOn(this.timer % this.strobe === this.strobePos);
        } else {
          this.setOn(this.timer % this.strobe === 0);
        }
        this.timer++;
      }
    }
  }, Box.prototype);

  var Door = function() {
    Obj.apply(this, arguments);
    this.invisible = false;
    this.isOpen = this.isOpen === undefined ? false : this.isOpen;
    this.passable = false;
    this.transform = this.isOpen ? this.openTransform : this.closeTransform;

    this._calculateRealTransform();
    this._update();
  }

  Door.prototype = _.defaults({
    draw: function(context) {
      context.beginPath();
      context.fillStyle = '#a0a0a0';
      context.fillStyle = '#808080';
      context.rect(-this.width / 2, -this.height / 2, this.width, this.height);
      context.stroke();
      context.fill();

      var overlay = this.world.contexts.overlay;
      overlay.save();
      overlay.transform(this.realTransform[0][0], this.realTransform[0][1], this.realTransform[1][0], this.realTransform[1][1], this.realTransform[0][2], this.realTransform[1][2]);

      if (this.sealed) {
        overlay.fillStyle = '#f0f0f0';
      } else if (this.isOpen) {
        overlay.fillStyle = '#10f010';
      } else {
        overlay.fillStyle = '#f01010';
      }

      overlay.beginPath();
      overlay.moveTo(-this.hWidth, -this.hHeight + 2);
      overlay.lineTo(this.hWidth, -this.hHeight + 3);
      overlay.lineTo(this.hWidth, -this.hHeight + 5);
      overlay.lineTo(-this.hWidth, -this.hHeight + 4);
      overlay.lineTo(-this.hWidth, -this.hHeight + 2);
      overlay.fill();

      overlay.beginPath();
      overlay.moveTo(-this.hWidth, -this.hHeight + 7);
      overlay.lineTo(this.hWidth, -this.hHeight + 8);
      overlay.lineTo(this.hWidth, -this.hHeight + 10);
      overlay.lineTo(-this.hWidth, -this.hHeight + 9);
      overlay.lineTo(-this.hWidth, -this.hHeight + 7);
      overlay.fill();

      overlay.beginPath();
      overlay.moveTo(-this.hWidth, this.hHeight - 2);
      overlay.lineTo(this.hWidth, this.hHeight - 3);
      overlay.lineTo(this.hWidth, this.hHeight - 5);
      overlay.lineTo(-this.hWidth, this.hHeight - 4);
      overlay.lineTo(-this.hWidth, this.hHeight - 2);
      overlay.fill();

      overlay.beginPath();
      overlay.moveTo(-this.hWidth, this.hHeight - 7);
      overlay.lineTo(this.hWidth, this.hHeight - 8);
      overlay.lineTo(this.hWidth, this.hHeight - 10);
      overlay.lineTo(-this.hWidth, this.hHeight - 9);
      overlay.lineTo(-this.hWidth, this.hHeight - 7);
      overlay.fill();

      overlay.restore();
    },

    open: function() {
      this.isOpen = true;
      this.transform = this.openTransform;
      this._calculateRealTransform();
      this._update();
    },

    close: function() {
      this.isOpen = false;
      this.transform = this.closeTransform;
      this._calculateRealTransform();
      this._update();
    }}, Obj.prototype);

    var ForceField = function() {
      Obj.apply(this, arguments);
      this.invisible = false;
      this.isOn = true;
      this.passable = false;
    };

    ForceField.prototype = _.defaults({
      _jitter: function(points) {
        var result = [];
        for (var i = 0; i < points.length - 1; i++) {
          var start = points[i], end = points[i + 1];
          result.push(start);
          if ((Math.abs(start.x - end.x) > 4) || (Math.abs(start.y - end.y) > 4)) {
            var xmid = (start.x + end.x + Math.random() * 8 - 4) /2;
            var ymid = (start.y + end.y + Math.random() * 8 - 4) /2;
            result.push({ x: xmid, y: ymid });
          }
          result.push(end);
        }
        return result;
      },
      draw: function(context) {

        var x1 = -this.hWidth, x2 = this.hWidth, y1 = -this.hHeight, y2 = this.hHeight;

        context.beginPath();
        if (this.passable) {
          context.strokeStyle = 'rgba(255, 64, 64, 0.2)';
          context.fillStyle = 'rgba(128, 64, 64, 0.2)';
        } else {
          context.strokeStyle = '#802020';
          context.fillStyle = 'rgba(255, 80, 64, 0.6)';
        }
        context.rect(x1, y1, this.width, this.height);
        context.stroke();
        context.fill();

        if (this.isOn) {
          var count = Math.min(this.width / 40, 4);
          x1+=2;
          y1+=2;
          x2-=2;
          y2-=2;
          var points = [ { x: x1, y: y1 }, { x: x2, y: y1 }, { x: x2, y: y2 }, { x: x1, y: y2 }, { x: x1, y: y1 }];
          for (var i = 0; i < count; i++) {
            points = this._jitter(points);
          }

          points.sort(function(a, b) {
            return a.x - b.x;
          });

          var overlay;
          if (this.world.lighting) {
            overlay = this.world.contexts.shadow;
            overlay.save();
            overlay.globalCompositeOperation = 'lighter';
          } else {
            overlay = this.world.contexts.overlay;
            overlay.save();
          }

          overlay.beginPath();
          overlay.lineWidth = 1;
          overlay.transform(this.realTransform[0][0], this.realTransform[0][1],
                            this.realTransform[1][0], this.realTransform[1][1],
                            this.realTransform[0][2], this.realTransform[1][2]);
          overlay.strokeStyle = '#ff0000';
          overlay.moveTo(points[0].x, points[0].y);
          for (var i = 1; i < points.length; i++) {
            overlay.lineTo(points[i].x, points[i].y);
          }
          overlay.stroke();
          overlay.restore();
        }
      },
      onContact: function(what, world) {
        if (what.name === 'player' && this.isOn) {
          world.lose();
        }
      }
    }, Obj.prototype);

    var Goal = function() {
      Obj.apply(this, arguments);
    }

    Goal.prototype = _.defaults({
      onCovered: function(what, world) {
        if (what.name === 'player') {
          world.win();
        }
      },
      passable: true,
      invisible: true
    }, Obj.prototype);

    NS.Goal = Goal;
    NS.ForceField = ForceField;
    NS.Button = Button;
    NS.Switch = Switch;
    NS.Door = Door;
    NS.Light = Light;
    NS.Crate = Crate;

}(window));
